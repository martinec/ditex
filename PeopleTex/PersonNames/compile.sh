#!/bin/sh

# Replace with your local unitex directory
UNITEX_DIR=/usr/local/Unitex
UNITEX_APP=$UNITEX_DIR/App
UNITEX_LNG=$UNITEX_DIR/ling 

SRC_DIC=./src
SRC_BIN=./bin
SRC_DIS=./dist
DIC_EXT=.dic

for file in $SRC_DIC/*$DIC_EXT ; do
	filename=$(basename $file)
	extension=${filename##*.}
    filename=${filename%.*}

	DIC_NAME=$filename 
	
	DIC_SRC=$SRC_DIC/$DIC_NAME.dic
	DIC_U16=$SRC_DIC/$DIC_NAME'_utf16.dic'
	DIC_BIN=$SRC_BIN/$DIC_NAME.bin
	DIC_INF=$SRC_BIN/$DIC_NAME.inf
	DIC_ZIP=$SRC_DIS/$DIC_NAME.zip

	echo "------------------------------------------------------------";
	echo "Compiling: $DIC_NAME"

	if [ ! -d $SRC_BIN ]; then
		mkdir $SRC_BIN
	fi

	$UNITEX_APP/Convert  -sUTF-8 -dUTF16-LE --sd="_utf16" $DIC_SRC
	$UNITEX_APP/SortTxt  $DIC_U16 -o $UNITEX_LNG/English/Alphabet_sort.txt
	$UNITEX_APP/Compress $DIC_U16 -o $DIC_BIN

	if [ ! -d $SRC_DIS ]; then
		mkdir $SRC_DIS
	fi

	zip -j $DIC_ZIP $DIC_BIN $DIC_INF ./LICENSE.txt ./README.txt

	rm $DIC_U16
	
done


