#----------------------------------------------------------------------
# DiTex-PersonalNames 2012
#----------------------------------------------------------------------
# Personal Names Unitex Dictionary 
# Latin Version
# LGPLLR http://www.ditex.org/lgpllr
#----------------------------------------------------------------------
# DiTex Project 
# http://www.ditex.org
#----------------------------------------------------------------------
# Dictionary of given names DiTex-Given.dic     v1.70
# Dictionary of surnames    DiTex-Surnames.dic  v1.70
#----------------------------------------------------------------------
# Authors :
# Cristian Martinez				martinec  at esiee.fr
#----------------------------------------------------------------------
# Contributors :
# Tita Kyriacopoulou			tita 	    at univ-mlv.fr
# Claude Martineau 				martinea  at univ-mlv.fr
#----------------------------------------------------------------------
Grammatical/Semantic codes used in dictionary:

+N		    = Name
+Hum 	    = Anthroponym
+Surname  = Surname
+Given 	  = Given name
+Norm     = Normalized form
+Prefix   = Name prefix
+Poly     = Polysemous name
:ms		    = masculine singular
:fs		    = feminine singular
:ms:fs    = neutral, masculine or feminine singular

Examples:

- Márquez,.N+Hum+Surname+Norm=marquez
- Adrián,.N+Hum+Given+Norm=adrian:ms
- Washington,.N+Hum+Surname+Norm=washington+Poly
- Łózińskiego,.N+Hum+Surname+Norm=lozinskiego
- van de Graaff,.N+Hum+Surname+Norm=de graaff van+Prefix=van_de_	
 
