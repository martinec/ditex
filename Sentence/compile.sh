#!/bin/sh

SEN_NAME=DiTex-Sentence-en
SRC_DIS=./dist

if [ ! -d $SRC_DIS ]; then
	mkdir $SRC_DIS
fi

SEN_ZIP=$SRC_DIS/$SEN_NAME.zip
zip $SEN_ZIP ./en/*.grf ./en/LICENSE.txt ./en/README.txt

