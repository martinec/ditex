#----------------------------------------------------------------------
# DiTex Automatic English Sentence Segmenter
#----------------------------------------------------------------------
# LGPLLR http://www.ditex.org/lgpllr
#----------------------------------------------------------------------
# DiTex Project 
# http://www.ditex.org
#----------------------------------------------------------------------
# Cristian Martinez (c) 2011 
# martinec at esiee.fr
#----------------------------------------------------------------------

Copy DiTex-Sentence-en directory in your /Workspace/English/Graphs/Preprocessing
