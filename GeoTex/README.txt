#----------------------------------------------------------------------
# DiTex-GeoTex Dictionaries
#----------------------------------------------------------------------
# Toponym Unitex Dictionary 
# Latin Version 1.0 Beta
# LGPLLR http://www.ditex.org/lgpllr
#----------------------------------------------------------------------
# This work uses data from Geonames.org project 
# under a Creative Commons Attribution 3.0 License
# see http://creativecommons.org/licenses/by/3.0/
#----------------------------------------------------------------------
# DiTex Project 
# http://www.ditex.org
#----------------------------------------------------------------------
# Cristian Martinez (c) 2011 
# martinec at esiee.fr
#----------------------------------------------------------------------

Grammatical/Semantic codes used in dictionary:

+N		 = Name
+PR		 = Proper Name
+Toponym = Toponym

+Country   = Toponym is a country
+City	   = Toponym is a city
+Capital   = Toponym is a capital of a political entity
+Populated = Toponym is a populated place

+gISO	 = ISO-3166 2-letter country code
+gLANG   = ISO 639-1 language code
+gLAT	 = Latitude in decimal degrees
+gLONG	 = Longitude in decimal degrees 
